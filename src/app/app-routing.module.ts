import { NgModule, Component } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { MainComponent } from './main/main.component';
import { TestComponent } from './test/test.component';
import { RouterGuardService } from './router-guard.service';
import { JoinComponent } from './join/join.component';
import { ArrowFuncComponent } from './arrow-func/arrow-func.component';
import { ParentComponent } from './parent/parent.component';

const routes: Routes = [
  {
    path:'',
    component:MainComponent,
    canActivate:[RouterGuardService]
  },
  {
    path:'login',
    component:LoginComponent
  },
  {
    path:'test',
    component:TestComponent,
    canActivate:[RouterGuardService]
  },
  {
    path:'join',
    component:JoinComponent
  },
  {
    path:'a',
    component:ArrowFuncComponent
  },
  {
    path:'parent',
    component:ParentComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
