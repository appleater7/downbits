import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-arrow-func',
  templateUrl: './arrow-func.component.html',
  styleUrls: ['./arrow-func.component.css']
})
export class ArrowFuncComponent implements OnInit {
  test:string = '난 컴포넌트';
  constructor() { }

  ngOnInit() {
  }
  callFunc1():void{
    function func(obj) {
      alert(obj.test);
    }
    func(this);
  }
  callFunc2():void{
    function func() {
      alert(this.test);
    }
    var f = func.bind(this);
    f();
    // bind의 역할 var f 가 바라보는 this 와 func1 이 바라보는 this
    // 를 동기화시킴
  }
  callFunc3():void{
    function func() {
      alert(this.test);
    }
    func.call(this);
    // bind 하는 동시에 실행됨!!
  }
  callFunc4():void{
    var func =()=>{
      alert(this.test);
      alert(this.callFunc3);
    }
    func();
  }
}