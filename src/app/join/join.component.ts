import { Component, OnInit } from '@angular/core';
import { UseExistingWebDriver } from 'protractor/built/driverProviders';
import { User } from '../login/user';
import { LoginService } from '../login/login.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-join',
  templateUrl: './join.component.html',
  styleUrls: ['./join.component.css']
})
export class JoinComponent implements OnInit {
  user:User = new User();
  constructor(private _ls:LoginService, private _rs:Router) { }

  ngOnInit() {
  }
  doJoin() {
    this._ls.join(this.user).subscribe(res=>{
      if (res.response == 1){
        alert('등록 성공');
        this._rs.navigate(['']);
      } else {
        alert('등록 실패');
      }
    })
  }
}
