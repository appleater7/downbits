import { Component, OnInit } from '@angular/core';
import { LoginService } from '../login/login.service';
import { User } from '../login/user';
import { Router } from '@angular/router';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit {
  user:User = new User();
  users:User[]=[];
  constructor(private _ls:LoginService, private _rs:Router) {
    this.user.uiId = localStorage.getItem('id');
    // this._ls.findList(this.user).subscribe(res=>{
    //   this.users = res.response;
    // });
    this._ls.findList22222().subscribe(res2=>{
      console.log(res2);
      // this.users = <User[]>res2;
    });
  }

  ngOnInit() {
  }
  doLogout() {
    localStorage.removeItem('id');
    localStorage.removeItem('pwd');
    this._rs.navigate(['test']);
    // localStorage.clear();
  }
}
