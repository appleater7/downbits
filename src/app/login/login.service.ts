import { Injectable } from '@angular/core';
import { User } from './user';
import { ajax } from 'rxjs/ajax';
import { HttpClient, HttpHeaders } from '@angular/common/http'

@Injectable({
  providedIn: 'root'
})
export class LoginService {
  private headers:HttpHeaders;
private headersAjax() {
    return {
    'Content-Type':'application/json',
    'X-AUTH-ID': localStorage.getItem('id'),
    'X-AUTH-TOKKEN': localStorage.getItem('token')
    }
  }
  
  constructor(private _http:HttpClient) { }

  login(user:User){
    return ajax.post('http://localhost:88/login',user,this.headersAjax());    
  }
  findList(user:User){
    var tokken = localStorage.getItem('token');
    // var param = '?tokken='+tokken;
    // console.log(param);
    return ajax.get('http://localhost:88/userinfos',this.headersAjax());
  }
  findList22222() {    
    // this.headers = new HttpHeaders();
    // const headers = this.headers;
    return this._http.get('http://localhost:88/userinfos');
  }
  findUser(user:User){
    var uiId = localStorage.getItem('id');
    var tokken = localStorage.getItem('token');
    var param = '?uiId='+uiId;
    // console.log(param);
    return ajax.get('http://localhost:88/userinfo'+param,this.headersAjax());
  }
  join(user:User){
    var tokken = localStorage.getItem('token');
    return ajax.post('http://localhost:88/join',user,this.headersAjax());
  }
  // 선생님 코드
  // private base_url:string='http://localhost:88';
  // private headers:HttpHeaders;
  // constructor(private _http:HttpClient) {
    
  //   var uiId = localStorage
  //   .getItem('uiId');
  //   var tokken = localStorage
  //   .getItem('tokken');
  //   this.headers = new  HttpHeaders().set('x-auth-id',uiId).set('x-auth-tokken',tokken);
  // }
  
  // login(user:User){
  //   return this._http.post(this.base_url+'/login',user);
  // }

  // findList(){
  //   const headers = this.headers;
  //   return this._http
  //   .get(this.base_url+'/userinfos',{headers});
  // }
  // signin(user:User){
  //   return this._http.post(this.base_url+'/signin',user);
  // }
  
}
