import { Component, OnInit } from '@angular/core';
import { LoginService } from './login.service';
import { User } from './user';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  
})
export class LoginComponent implements OnInit {
  id:string = '';
  pwd:string = '';
  isLogin:boolean = false;
  user:User = new User();
  constructor(private _ls:LoginService, private _rs:Router) { 

  }
  ngOnInit() {
  }
  // doLogin2() {
  //   // console.log(`id : ${this.id}`);
  //   // console.log(`pwd : ${this.pwd}`);
  //   // console.log('id : ' + this.id);
  //   // console.log('id : ' + this.pwd);
  //   this._ls.login(this.user).subscribe(res=>{
  //     console.log(res.response);
  //     if(res.response===1){
  //       this.isLogin = true;
  //       if(this.isLogin){
  //         localStorage.setItem('id',this.user.uiId);
  //         localStorage.setItem('pwd',this.user.uiPwd);
  //       }
  //     }
  //   });
  // }  
  doLogin() {
    this._ls.login(this.user).subscribe(res=>{
      console.log(res);
      if(res){
        this.user = <User>res.response;
        if(this.user.tokken){
          this.isLogin = true;
          localStorage.setItem('id',this.user.uiId);
          localStorage.setItem('token',this.user.tokken);
          this.findList();
          this._rs.navigate(['']);
        }
      }
    })
  }
  findList() {
    this._ls.findList22222().subscribe(res=>{
      console.log(res);
    })
  }
  goPage(url) {
    this._rs.navigate([url]);
  }
}


