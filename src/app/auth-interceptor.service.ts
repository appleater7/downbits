import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpEvent, HttpHandler, HttpRequest, HttpHeaders } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthInterceptorService implements HttpInterceptor{

  constructor(private _router:Router) { }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    var uiId = localStorage.getItem('uiId');
    var tokken = localStorage.getItem('token');
    var headers:HttpHeaders = new HttpHeaders();
    console.log(req);
    if (req.url.indexOf('login')==-1 && !tokken){
      this._router.navigate(['/login']);
      return throwError('Auth Error');
    }else if(tokken) {
      headers = new HttpHeaders()
      .set('x-auth-id',uiId)
      .set('x-auth-tokken',tokken);
    }
    const authReq = req.clone({headers});
    return next.handle(authReq);
  }
}
